window.addEventListener('load', function() { //počaka da se stvar naloži, šele potem začne izvajati kodo, zelo dobra praksa
	//stran nalozena
	//hiter test
		
	// izvedi prijavo
	var izvediPrijavo = function(){
		var uporabnik = document.querySelector("#uporabnisko_ime").value; // naslalvljamo spr z imenom #ime, .value pridobi vrednost
		//.value da vrednost iz <value Type="text" Value="Dejan"> ; za izmed pa .Innerhtml
		document.querySelector("#uporabnik").innerHTML=uporabnik;
		document.querySelector(".pokrivalo").style.visibility="hidden"; // če ima gradnik atribut class, ga naslavljam z piko (.)
	}
	
	document.querySelector("#prijavniGumb").addEventListener('click', izvediPrijavo); //dodamo kdaj se izvede funkcija
	
	//dodaj opomnik
	var dodajOpomnik = function(){
	
		var naziv_opomnika=document.querySelector("#naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		var cas_opomnika=document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value="";
		
		var opomniki=document.querySelector("#opomniki");
		opomniki.innerHTML += " \
			<div class='opomnik senca rob'> \
				<div class='naziv_opomnika'>"+naziv_opomnika +
				"</div> \
				<div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika + "</span> sekund.</div> \
			</div>";
		
	}
	
	document.querySelector("#dodajGumb").addEventListener('click',dodajOpomnik);
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			
			if(cas==0){
				var naziv_opomnika=opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev"+naziv_opomnika+"'je potekla'");
				document.querySelector("#opomniki").removeChild(opomnik);
			}else {
				casovnik.innerHTML=cas-1;
			}
			
			}
	}
	setInterval(posodobiOpomnike, 1000); // vsako sekundo zažene opomnike
	
});